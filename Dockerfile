FROM jenkins/jenkins:latest
LABEL maintainer="mstewart@riotgames.com"

USER root
RUN apt-get update
RUN apt-get install -y maven
RUN apt-get install -y ansible
RUN apt-get install -y telnet
RUN apt-get install -y vim dos2unix
RUN mkdir /var/log/jenkins
RUN mkdir /var/cache/jenkins
RUN chown -R jenkins:jenkins /var/log/jenkins
RUN chown -R jenkins:jenkins /var/cache/jenkins
RUN mkdir -p /var/jenkins_home/.ssh
COPY id_rsa_jenkins_master /var/jenkins_home/.ssh/id_rsa
COPY id_rsa_jenkins_master.pub /var/jenkins_home/.ssh/id_rsa.pub
RUN chown -R jenkins:jenkins /var/jenkins_home/.ssh
RUN chmod 750 /var/jenkins_home/.ssh
RUN chmod 600 /var/jenkins_home/.ssh/id_rsa
USER jenkins
WORKDIR /var/jenkins_home

ENV JAVA_OPTS="-Xmx8192m"
ENV JENKINS_OPTS="--handlerCountMax=300 --logfile=/var/log/jenkins/jenkins.log --webroot=/var/cache/jenkins/war"